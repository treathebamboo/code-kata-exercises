require 'rubygems'
require 'pry'
require './gol'

describe "Gol" do
  before(:each) do
    @g = Gol::Game.new(5)
  end
  it "Generate a square world of dimension 36" do
    @g = Gol::Game.new(6)
    expect(@g.world.area).to eq(6**2)
  end

  it "kickstarts the universe" do
    @g.world.big_bang
    expect(@g.world.started?).to be_true
  end

  it "kickstarts the universe" do
    @g.world.big_bang
    expect(@g.world.started?).to be_true
  end

  it "the world contains cells" do
    @g.world.big_bang
    all_cells = true
    @g.world.field.each do |row|
      row.each do |cell|
        all_cells = false unless cell.instance_of?(Gol::Cell)
      end
    end
    expect(all_cells).to be_true
  end

  it "the world contains dead or alive cells" do
    cell = Gol::Cell.new
    expect([:dead,:alive]).to include(cell.status)
  end

  it "begins at turn 0" do
    expect(@g.epoch).to eq(0)
  end

  it "evolves incrementing the epoch" do
    @g.evolve
    expect(@g.epoch).to eq(1)
  end
end

describe "Cells" do
  before(:each) do
    @g = Gol::Game.new(5)
  end

  it "knows its neighbourhood" do
    @g.wo
  end
end
