require 'rubygems'
require 'pry'

module Gol
  class Game
    attr_reader :world, :epoch
    def initialize(size)
      @epoch = 0
      @world = World.new(size)
    end

    def evolve
      @epoch += 1
    end
  end

  class World
    attr_reader :field
    def initialize(size)
      @size = size
      @field = [[nil]*size]*size
      @started = false
    end

    def area
      @size**2
    end

    def big_bang
      @field = [[Cell.new]*@size]*@size
      @started = true
    end

    def started?
      @started
    end
  end

  class Cell
    STATES = [:dead,:alive]

    attr_reader :status
    def initialize(x,y)
      @status = STATES.sample
      @position = [x,y]
    end
  end
end
