require 'rubygems'
require 'pry'

class Weather
  attr_reader :file_path, :weather_struct

  def open_file
    File.open(@file_path, 'r')
  end

  def parse_file
    self.open_file.each do |line|
      parsed_line = parse_line(line)
      next unless parsed_line
      @weather_struct << parsed_line
    end
    @weather_struct
  end

  def parse_line(line)
    if line.strip.empty?
      return false
    end
    tmp_ret = line.strip.split(/\s+/)[0..2]
    return tmp_ret.join("").match(/[^\d|^\*]/) ? false : tmp_ret.map { |e| e.gsub('*', '').to_i }
  end

  def initialize(file_path)
    @file_path = file_path
    @weather_struct = []
  end

  def run
    parse_file
    get_min
  end

  def calculate_spread(array)
    return (array[2] - array[1]).abs
  end

  def get_min(struct = nil)
    struct ||= @weather_struct
    day = nil
    min_spread = 9999999
    struct.each do |e|
      c = calculate_spread(e)
      if c <= min_spread
        min_spread = c
        day = e[0]
      end
    end
    day
  end
end
