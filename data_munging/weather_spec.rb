require 'rubygems'
require 'pry'
require './weather'

describe Weather, 'exists' do
  before(:each) do
    @w = Weather.new('weather.dat')
  end

  it "returns not nil if the file exists" do
    expect(@w.open_file).not_to eq(nil)
  end

  it "returns a File if the file exists" do
    expect(@w.open_file).to be_instance_of(File)
  end
end

describe Weather, "Parsable" do
  before(:each) do
    @w = Weather.new('weather.dat')
  end

  it "returns an array" do
    expect(@w.parse_file).to be_instance_of(Array)
  end

  it "returns an array of non empty arrays " do
    all_right = true
    weather_struct = @w.parse_file
    if weather_struct.empty?
      all_right = false
    else
      weather_struct.each do |e|
        all_right = false unless e.instance_of?(Array) && e.any?
      end
    end
    expect(all_right).to be_true
  end

  it "returns and array of arrays of 3 elements" do
    all_right = true
    weather_struct = @w.parse_file

    weather_struct.each do |e|
      all_right = false if e.size != 3
    end

    expect(all_right).to be_true
  end

  it "parses the lines without returning nil" do
    line = ""
    expect(@w.parse_line(line)).not_to be_nil
  end

  it "parses the lines returning false if the line is empty" do
    line = ""
    expect(@w.parse_line(line)).to be_false
  end

  it "returns false if the line contains a not digit character" do
    line = "  27  91d    72    82          69.7       0.00 RTH     250 12.1 230  17  7.1  90 47 1009.0"
    expect(@w.parse_line(line)).to be_false
  end

  it "returns false when reading the first line" do
    line = "  Dy MxT   MnT   AvT   HDDay  AvDP 1HrP TPcpn WxType PDir AvSp Dir MxS SkyC MxR MnR AvSLP"
    expect(@w.parse_line(line)).to be_false
  end

  it "returns false when reading the second line" do
    line = "mo  82.9  60.5  71.7    16  58.8       0.00              6.9          5.3"
    expect(@w.parse_line(line)).to be_false
  end

  it "returns a correctly parsed line" do
    line = "  12  23* 34    43"
    expect(@w.parse_line(line)).to eq([12, 23, 34])
  end
end

describe Weather, "minimum spread" do
  before(:each) do
    @w = Weather.new('weather.dat')
  end

  it "calculates the spread" do
    expect(@w.calculate_spread([1, 10, 20])).to eq(10)
  end

  it "returns the day with the minimum spread in a range" do
    struct = [
      [1, 10, 20],
      [2, 10, 15]
    ]
    expect(@w.get_min(struct)).to eq(2)
  end
end
